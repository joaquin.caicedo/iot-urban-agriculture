#include "FS.h"
#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <NTPClient.h>
#include <WiFiUdp.h>
#include <DHT.h>
#include <TinyGPS++.h>
#include <SoftwareSerial.h>

#define DHTPIN D3
#define DHTTYPE DHT11

// Conexión
const char * ssid = "**********"; // Ingresar nombre de red wi-fi.
const char * password = "**********"; // Ingresar contraseña wi-fi.

static const int RXPin = D6, TXPin = D5, inHumedad = A0, inBomba = D7, inVentilador = D8, outBomba = D0, outVentilador = D1; // Definición de pines.
static const uint32_t GPSBaud = 9600; // Tasa de baudios del GPS.
DHT dht(DHTPIN, DHTTYPE); // Definición del pin y tipo de sensor de temperatura.

WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP, "pool.ntp.org");

TinyGPSPlus gps; // Objeto TinyGPS++.

SoftwareSerial ss(RXPin, TXPin); // Conexión serial al GPS.

const char * AWS_endpoint = "**********"; // Ingresar punto de enlace de AWS - MQTT broker ip

void callback(char * topic, byte * payload, unsigned int length)
{
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) 
  {
    Serial.print((char) payload[i]);
  }
  Serial.println();
}

WiFiClientSecure espClient;
PubSubClient client(AWS_endpoint, 8883, callback, espClient); //MQTT puerto 8883 - estándar.
long lastMsg = 0;
char msg[250];
int value = 0;

void setup_wifi() 
{
  delay(10);
  // Inicializamos Wifi
  espClient.setBufferSizes(512, 512);
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) 
  {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  timeClient.begin();
  while (!timeClient.update()) 
  {
    timeClient.forceUpdate();
  }
  espClient.setX509Time(timeClient.getEpochTime());
}

void reconnect() 
{
  // Reconexión
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    if (client.connect("ESPthing")) {
      Serial.println("connected");

      client.publish("outTopic", "hello world");
      
      client.subscribe("inTopic");
    } else 
    {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      char buf[256];
      espClient.getLastSSLError(buf, 256);
      Serial.print("WiFiClientSecure SSL error: ");
      Serial.println(buf);
      delay(5000);
    }
  }
}

void setup() 
{
  Serial.begin(9600);
  ss.begin(GPSBaud);
  Serial.setDebugOutput(true);
  pinMode(LED_BUILTIN, OUTPUT);
  
  pinMode(inHumedad, INPUT); // Humedad input.
  pinMode(inBomba, INPUT); // Control bomba input.
  pinMode(inVentilador, INPUT); // Control ventilador input.
  pinMode(outBomba, OUTPUT); // Control bomba output.
  pinMode(outVentilador, OUTPUT); // Control ventilador output.
  
  dht.begin();
  
  setup_wifi();
  delay(1000);
  if (!SPIFFS.begin()) 
  {
    Serial.println("Failed to mount file system");
    return;
  }
  Serial.print("Heap: ");
  Serial.println(ESP.getFreeHeap());
  
  // Cargamos los certificados
  File cert = SPIFFS.open("/cert.der", "r"); // Verifica
  if (!cert) 
  {
    Serial.println("Failed to open cert file");
  } else
    Serial.println("Success to open cert file");
  delay(1000);
  if (espClient.loadCertificate(cert))
    Serial.println("cert loaded");
  else
    Serial.println("cert not loaded");
    
  // Cargamos llave privada
  File private_key = SPIFFS.open("/private.der", "r"); 
  if (!private_key) 
  {
    Serial.println("Failed to open private cert file");
  } else
    Serial.println("Success to open private cert file");
  delay(1000);
  if (espClient.loadPrivateKey(private_key))
    Serial.println("private key loaded");
  else
    Serial.println("private key not loaded");
    
  // Cargamos CA 
  File ca = SPIFFS.open("/ca.der", "r"); 
  if (!ca) 
  {
    Serial.println("Failed to open ca ");
  } else
    Serial.println("Success to open ca");
  delay(1000);
  if (espClient.loadCACert(ca))
    Serial.println("ca loaded");
  else
    Serial.println("ca failed");
  Serial.print("Heap: ");
  Serial.println(ESP.getFreeHeap());
}

void loop() 
{
  if (!client.connected()) {
    reconnect();
  }
  client.loop();

  while (ss.available() > 0)
    if (gps.encode(ss.read())) // Lectura del GPS.
    {
      float latitud = 0;
      float longitud = 0;
      
      if (gps.location.isValid())
      {
        float latitud = gps.location.lat(); // Se obtiene la latitud.
        float longitud = gps.location.lng(); // Se obtiene la longitud.
      }
      
      int dia = 0;
      int mes = 0;
      int anio = 0;
      
      if (gps.date.isValid())
      {
        dia = gps.date.day(); // Se obtiene el día.
        mes = gps.date.month(); // Se obtiene el mes.
        anio = gps.date.year(); // Se obtiene el año.
      }
    
      if (gps.time.isValid())
      {
        int hora = gps.time.hour(); // Se obtiene la hora.
        if(hora < 5)
          hora += 19;        
        else 
          hora -= 5;
        int minuto = gps.time.minute(); // Se obtiene los minutos.
        int segundo = gps.time.second(); // Se obtiene los segundos.

        String sdia = String(dia); // Día a cadena de texto.
        String smes = String(mes); // Mes a cadena de texto.
        String sanio = String(anio); // Año a cadena de texto.
        String shora = String(hora); // Hora a cadena de texto.
        String sminuto = String(minuto); // Minuto a cadena de texto.
        String ssegundo = String(segundo); // Segundo a cadena de texto.

        if(mes < 10)
          smes = "0" + smes;
        if(dia < 10)
          sdia= "0" + sdia;
        if(hora < 10)
          shora = "0" + shora;
        if(minuto < 10)
          sminuto = "0" + sminuto;
        if(segundo < 10)
          ssegundo = "0" + ssegundo;

        String fecha = sanio + "-" + smes + "-" + sdia + "T" + shora + ":" + sminuto + ":" + ssegundo; // Se concatena la fecha. 

        float temperatura = dht.readTemperature(); // Lectura de temperatura.
        int humedad = analogRead(inHumedad); // Lectura de humedad.
        humedad = (1024 - humedad)*100/1024; // Normalización a 100 de la humedad.
        
        if(isnan(temperatura))
        {
          temperatura = 0;
        }
        
        if(humedad > 100)
        {
          humedad = 100;
        }

        // ACTUADORES
        bool bomba = digitalRead(inBomba); // Activación del control de la bomba de riego.
        bool ventilador = digitalRead(inVentilador); // Activación del control del ventilador.
        String bomba_estado = "off"; // Indicador del estado de la bomba de riego.
        String ventilador_estado = "off"; // Indicador del estado del ventilador.

        // Activación de la bomba de riego
        if (bomba == HIGH)
        {
          if (humedad < 50) // Humedad máxima para la activación.
          {
            digitalWrite(outBomba, HIGH);
            bomba_estado = "on";
          }
          else digitalWrite(outBomba, LOW);
        } else digitalWrite(outBomba, LOW);
        
        // Activación del ventilador
        if (ventilador == HIGH)
        {
          if (temperatura > 15) // Temperatura mínima de activación.
          {
            digitalWrite(outVentilador, HIGH);
            ventilador_estado = "on";
          }
          else digitalWrite(outVentilador, LOW);
        } else digitalWrite(outVentilador, LOW);

        // MENSAJES DE SALIDA
        Serial.print(F("Fecha: "));
        Serial.println(fecha);
        
        Serial.print("Temperatura: ");
        Serial.print(temperatura);
        Serial.println(" °C");
        
        Serial.print("Humedad: ");
        Serial.print(humedad);
        Serial.println(" %");

        Serial.print(F("Latitud: ")); 
        Serial.println(latitud, 6);
        
        Serial.print(F("Longitud: "));
        Serial.println(longitud, 6);

        Serial.print("Riego: ");
        Serial.print(bomba_estado);

        Serial.print("Ventilación: ");
        Serial.print(ventilador_estado);
        
        //  ENVÍO DE MENSAJE JSON AL TOPIC DE AWS
        snprintf (msg, 250, "{\"fecha\": \"%s\", \"id_granja\": \"c94da380-5464-11ec-9189-afe07e8a57cc\", \"temperatura\": %.2f, \"humedad\": %i, \"latitud\": %.6f, \"longitud\": %.6f, \"riego\": \"%s\",\"ventilación\": \"%s\"}", fecha.c_str(), temperatura, humedad, latitud, longitud, bomba_estado.c_str(), ventilador_estado.c_str());
        client.publish("granja1/inTopic", msg);
      }
      else
      {
        Serial.print(F("INVÁLIDO"));
      }
    
      Serial.println();
      digitalWrite(LED_BUILTIN, LOW);
      delay(1000);
      digitalWrite(LED_BUILTIN, HIGH);
      delay(9000);
    }

  if (millis() > 5000 && gps.charsProcessed() < 10)
  {
    Serial.println(F("No se ha dectectado el GPS."));
    while(true);
  }
}
